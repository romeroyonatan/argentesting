# ¿Por qué Python está buenísimo?

> Argentesting 2019

## Caso práctico

### Archivo hosts

El archivo hosts de un ordenador es usado por el sistema operativo para guardar
la correspondencia entre dominios de Internet y direcciones IP. Este es uno de
los diferentes métodos que usa el sistema operativo para resolver nombres de
dominios. [Wikipedia][Archivo_hosts]


### Ejercicio

En el archivo [etchosts.py][script] hay un _script_ que nos permite asociar IPs
a nombres de dominio. Este programa ya exitía en nuestra organización, sin
embargo nos piden cambiarlo, ya que actualmente, si el nombre de dominio ya
tiene una IP asignada, no lo actualiza con la nueva IP.

Además los usuarios quieren borrar asociaciones de nombre de dominio e IPs


En resumen lo que tenemos que hacer:

1. Si el registro a agregar ya existe, modificarlo con la nueva IP
1. Eliminar registros

Agregué un archivo hosts de ejemplo para no tocar la configuración de nuestras
computadoras

### Ejemplo de uso

```sh
$ python etchosts.py  -h
usage: etchosts.py [-h] ip_address name

positional arguments:
ip_address
name

optional arguments:
-h, --help  show this help message and exit
```

```sh
$ python etchosts.py 10.0.0.1 www.aol.com
INFO:root:No encontramos el nombre en el archivo, lo agregamos al final

$ cat hosts
127.0.0.1  localhost
::1     ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
10.0.0.1 www.aol.com
```


### El plan

Como no queremos romper nada que ya existe (regresión), vamos a crear los tests
unitarios que no tiene el script actual.

Para ello podriamos usar la [biblioteca built-in][unittest] que ya viene con
Python u [otras bibliotecas maduras que existen en el ecosistema de Python][testing].

En nuestro caso particular elegí usar [pytest][pytest] por su simplicidad para
escribir test unitarios usando sólo funciones. Ademas quiero mostrar cómo
funcionan los [fixtures][fixtures]


[Archivo_hosts]: https://es.wikipedia.org/wiki/Archivo_hosts
[script]: ./caso_practico/etchosts.py
[unittest]: https://docs.python.org/3/library/unittest.html
[testing]: https://wiki.python.org/moin/PythonTestingToolsTaxonomy
[pytest]: https://docs.pytest.org/
[fixtures]: https://docs.pytest.org/en/latest/fixture.html#fixture
