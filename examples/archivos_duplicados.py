import pathlib
import hashlib
import pprint

# hash md5 -> lista de archivos
hashmap = {}

for path in pathlib.Path(".").rglob("*.png"):
    content = path.read_bytes()
    hash_md5 = hashlib.md5(content).hexdigest()
    filelist = hashmap.setdefault(hash_md5, [])
    filelist.append(str(path))


pprint.pprint(
    {hash_md5: files for hash_md5, files in hashmap.items() if len(files) > 1}
)
