def mi_generador(años):
    print("Hoy tenés", años, "años")
    yield años
    años += 1
    print("El año que viene vas a tener", años, "años")
    yield años
    años += 10
    print("En una decada vas a tener", años, "años")
    return años
