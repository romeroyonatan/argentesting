import argparse
import logging
import subprocess
import tempfile

ETC_HOSTS = "./hosts"

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("ip_address")
    parser.add_argument("name")
    args = parser.parse_args()

    ip_address = args.ip_address
    name = args.name
    name_found = False

    with tempfile.NamedTemporaryFile("wt") as output_file:
        with open(ETC_HOSTS) as input_file:
            for line in input_file:
                line = line.strip()
                if line:
                    current_ip_address = line.split()[0]
                    if ip_address == current_ip_address:
                        logging.info("Encontramos la ip")
                        # si el nombre no existe para esa ip, lo adjuntamos al final
                        if name not in line:
                            logging.info("Se agrego el nombre a la IP")
                            output_file.write(line + " " + name + "\n")
                            name_found = True
                        else:
                            # el nombre ya esta configurado, lo dejamos como esta
                            logging.info(
                                "El nombre ya esta configurado con la IP correcta"
                            )
                            output_file.write(line + "\n")
                            name_found = True
                    else:
                        # guardamos la linea como esta
                        output_file.write(line + "\n")

        if not name_found:
            logging.info(
                "No encontramos el nombre en el archivo, lo agregamos al final"
            )
            output_file.write(ip_address + " " + name + "\n")

        # necesario para que se guarde el archivo en el disco
        output_file.flush()

        subprocess.check_output(["cp", output_file.name, ETC_HOSTS])


if __name__ == "__main__":
    main()
